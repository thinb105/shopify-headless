import { ApolloClient } from 'apollo-client';
import { InMemoryCache, HttpLink, NormalizedCacheObject } from 'apollo-boost';
import fetch from 'isomorphic-fetch';

let apolloClient: any;

if (!process.browser) {
  global.fetch = fetch;
}

const shopifyApiLink = new HttpLink({
  uri: 'https://thinb-test.myshopify.com/api/graphql',
  headers: {
    'X-Shopify-Storefront-Access-Token': '2a0d0d5d55fd223d06132b21bed49bde',
  },
});

function create(
  initialState: NormalizedCacheObject,
): ApolloClient<NormalizedCacheObject> {
  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    link: shopifyApiLink,
    cache: new InMemoryCache().restore(initialState || {}),
  });
}

export default function initApollo(
  initialState: NormalizedCacheObject,
): ApolloClient<NormalizedCacheObject> {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
