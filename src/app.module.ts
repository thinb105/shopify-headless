import nextjs from 'next';
import { RenderModule } from 'nest-next';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';

@Module({
  imports: [
    RenderModule.forRootAsync(
      nextjs({
        dev: process.env.NODE_ENV === 'development',
      }),
      {
        viewsDir: null,
      },
    ),
  ],
  controllers: [AppController],
})
export class AppModule {}
